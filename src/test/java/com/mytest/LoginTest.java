package com.mytest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.mytest.pages.DashboardPage;
import com.mytest.pages.LeftBar;
import com.mytest.pages.LoginPage;
import com.mytest.pages.ReceiptPage;
import com.mytest.utils.GlobalUtils;

/**
 * Tests Receipt Feature.
 * 
 * @author omoto
 *
 */
public class LoginTest {
    private LoginPage loginPage;
    private LeftBar leftBar;
    private DashboardPage dashboardPage;
    private ReceiptPage receiptPage;

    /**
     * Test Pre Conditions
     */
    @Before
    public void prepareTest() {

        loginPage = PageFactory.initElements(GlobalUtils.driver, LoginPage.class);

        GlobalUtils.driver.manage().window().maximize();

        GlobalUtils.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    /**
     * Test that Adds a New Receipt.
     */
    @Test
    public void testAddingNewReceipt() {

        // Logging in
        dashboardPage = loginPage.loginAs("sqainc@gmail.com", "Tester@123");

        // Looking for left bar
        leftBar = dashboardPage.accessLeftBar();

        // Accessing 'Receipt' page
        receiptPage = leftBar.accessReceiptPage();

        String description = "Venda de teste";

        String value = "99,99";

        boolean isPaid = true;

        // Creating a New Receipt
        receiptPage.createNewReceipt(description, value, isPaid);

        // Asserting if Transaction is Paid
        assertThat(receiptPage.isPaidTransaction(), is(isPaid));

        // Asserting Transaction Description
        assertThat(receiptPage.getDescription(), is(equalTo(description)));

        // Asserting Transaction Value
        assertThat(receiptPage.getValue(), is(equalTo(value)));

    }

    /**
     * Test Pos Conditions
     */
    @After
    public void quitTest() {

        GlobalUtils.driver.quit();

    }
}
