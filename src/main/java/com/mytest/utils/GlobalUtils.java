package com.mytest.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Class which holds all important parameters of tests
 * @author omoto
 *
 */
public class GlobalUtils {
	public static int GLOBAL_TIMEOUT = 10;	
	public static WebDriverWait wait;
	public static WebDriver driver;
	
	static {
		
		driver = new ChromeDriver();
		
		wait = new WebDriverWait(driver, GlobalUtils.GLOBAL_TIMEOUT); 
		
	}
}
