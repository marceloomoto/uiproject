package com.mytest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.mytest.utils.GlobalUtils;

/**
 * Page Object of "The Main Left" Bar
 * 
 * @author omoto
 *
 */
public class LeftBar {
    @FindBy(how = How.ID, using = "navServicesReceitas")
    private WebElement receitasButton;

    /**
     * Default Constructor
     */
    public LeftBar() {

        // Wait for bar to load
        GlobalUtils.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("navServices")));

    }

    /**
     * Method that Access "Receipt" Page
     * 
     * @return access Receipt Page
     */
    public ReceiptPage accessReceiptPage() {

        receitasButton.click();

        return PageFactory.initElements(GlobalUtils.driver, ReceiptPage.class);

    }
}
