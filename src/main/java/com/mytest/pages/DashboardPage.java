package com.mytest.pages;

import org.openqa.selenium.support.PageFactory;

import com.mytest.utils.GlobalUtils;

/**
 * Page Object of "Dashboard" Page
 * 
 * @author omoto
 *
 */
public class DashboardPage {

    /**
     * Default Constructor
     */
    public DashboardPage() {

        if (!GlobalUtils.driver.getTitle().contains("Resumo")) {
            throw new IllegalStateException("Error: Wrong page!");
        }

    }

    /**
     * Method that Access the Main Left Bar
     * 
     * @return
     */
    public LeftBar accessLeftBar() {

        return PageFactory.initElements(GlobalUtils.driver, LeftBar.class);

    }
}
