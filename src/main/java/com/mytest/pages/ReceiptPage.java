package com.mytest.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.mytest.utils.GlobalUtils;

/**
 * Page Object of "Receipt" Page
 * 
 * @author omoto
 *
 */
public class ReceiptPage {
    @FindBy(how = How.ID, using = "btnNewItem")
    private WebElement createNewReceiptButton;

    @FindBy(how = How.CSS, using = "tbody#transactionTableContent > tr:nth-child(1) > td:nth-child(2) > span")
    private WebElement effectiveSpan;

    @FindBy(how = How.CSS, using = "tbody#transactionTableContent > tr:nth-child(1) > td:nth-child(3) > input")
    private WebElement descriptionInput;

    @FindBy(how = How.CSS, using = "tbody#transactionTableContent > tr:nth-child(1) > td:nth-child(5) > div > input")
    private WebElement valueInput;

    @FindBy(how = How.CSS, using = "tbody#transactionTableContent > tr:nth-child(1) > td:nth-child(7) > label")
    private WebElement isPaidCheckbox;

    @FindBy(how = How.CSS, using = "tbody#transactionTableContent > tr:nth-child(1) > td:nth-child(8) > span > button")
    private WebElement insertButton;

    @FindBy(how = How.CSS, using = "tbody#transactionTableContent > tr:nth-child(1)")
    private WebElement newReceiptRow;

    /**
     * Default Constructor
     */
    public ReceiptPage() {

        PageFactory.initElements(GlobalUtils.driver, this);

        if (!GlobalUtils.driver.getTitle().contains("Movimentações da sua conta")) {
            throw new IllegalStateException("Error: Wrong page!");
        }

    }

    /**
     * Returns if Transaction isPaid
     * 
     * @return isEffective
     */
    public boolean isPaidTransaction() {
        return effectiveSpan.getAttribute("class").contains("ibp");
    }

    /**
     * Returns Description Text
     * 
     * @return description
     */
    public String getDescription() {
        return newReceiptRow.getAttribute("iptsuplier");
    }

    /**
     * Returns Value Text
     * 
     * @return value
     */
    public String getValue() {
        return newReceiptRow.getAttribute("iptprice");
    }

    /**
     * Method That Creates a New Receipt
     * 
     * @param description
     * @param value
     * @param isPaid
     */
    public void createNewReceipt(String description, String value, boolean isPaid) {

        createNewReceiptButton.click();

        descriptionInput.sendKeys(description);

        valueInput.sendKeys(value);

        if ((isPaid && !isPaidCheckbox.isSelected()) ||
                !isPaid && isPaidCheckbox.isSelected()) {
            isPaidCheckbox.click();
        }

        insertButton.click();

    }
}
