package com.mytest.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.mytest.utils.GlobalUtils;

/**
 * Page Object of "Login" Page
 * 
 * @author omoto
 *
 */
public class LoginPage {
    @FindBy(how = How.ID, using = "user_email")
    private WebElement emailInput;

    @FindBy(how = How.ID, using = "user_password")
    private WebElement passwordInput;

    @FindBy(how = How.ID, using = "btnEntrarAlt")
    private WebElement loginButton;

    /**
     * Default Constructor
     */
    public LoginPage() {

        GlobalUtils.driver.navigate().to("https://zeropaper.com.br/sign_in");

        if (!GlobalUtils.driver.getTitle().equals("Entre no QuickBooks ZeroPaper")) {
            throw new IllegalStateException("Error: Wrong page!");
        }

    }

    /**
     * Method that Types Username, Password and Submit Login
     * 
     * @param username
     * @param password
     * @return to ### page
     */
    public DashboardPage loginAs(String username, String password) {

        emailInput.sendKeys(username);

        passwordInput.sendKeys(password);

        loginButton.submit();

        return PageFactory.initElements(GlobalUtils.driver, DashboardPage.class);

    }
}
